#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import IPython.display as display


# In[ ]:


def __get_describe(x):
  print(x.describe())


# In[ ]:


def __get_info(x):
  print(x.info())


# In[ ]:


def __get_columns(x):
  print(x.columns)


# In[ ]:


def __get_menu(x):
    print("1. Describe")
    print("2. Info")
    print("3. Columns")
    print("4. Salir")

    opcion = input("Seleccione una opción: ")

    if opcion == "1":
      __get_describe(x)
      opcion = input("Enter Para Borrar")
      display.clear_output(wait=True)
    elif opcion == "2":
      __get_info(x)
      opcion = input("Enter Para Borrar")
      display.clear_output(wait=True)
    elif opcion == "3":
      __get_columns(x)
      opcion = input("Enter Para Borrar")
      display.clear_output(wait=True)
    elif opcion == "4":
      opcion = input("Enter Para Borrar")
      display.clear_output(wait=True)
      return False
    else:
        print("Opción no válida")
    
    return True


# In[ ]:


def Main_EDA(datos):
  continuar = True  
  while continuar:
    continuar = __get_menu(datos)

